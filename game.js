class game { 
    constructor () {
        this.divSet = [
            {
                'name' : 'div1',
                'nbr' : '😁'
            },
            {
                'name' : 'div2',
                'nbr' : '👣'
            },
            {
                'name' : 'div3',
                'nbr' : '👀'
            },
            {
                'name' : 'div4',
                'nbr' : '💀'
            },
            {
                'name' : 'div5',
                'nbr' : '🤢'
            },
        ];

        this.deck = document.querySelector('#game');
        this.grid = document.createElement('section');
        this.grid.setAttribute('class', 'grid');
        this.deck.appendChild(this.grid);

        this.count = 0;

        //variables pour le compteur
        this.moves = 0;
        this.counter = document.querySelector('.nbMooves');
        this.counter.innerHTML = "Nombre de coups 0"
        //variables pour le timer
        this.second = 0;
        this.minute = 0;
        this.hour = 0;
        this.timer = document.querySelector('.timer');
        this.timer.innerHTML = "Temps écolé 0:0";
        this.interval;

        //variables pour les choix
        this.firstChoice = '';
        this.secondChoice = '';
        this.previousTarget = null;

        //déclaration de la variable por récupérer toutes les cartes qui match
        this.matchedCards = document.getElementsByClassName('match');

        this.cardsGame = this.divSet.concat(this.divSet).sort(() => 0.5 - Math.random());

        this.cardsGame.forEach(item =>{
            let card = document.createElement('div');
            card.classList.add('card');

            card.dataset.type = item.name;
            //création de la div .front
            let front = document.createElement('div');
            front.classList.add('front');
            //création de la div .back
            let back = document.createElement('div');
            back.classList.add('back');
            
          	//Création de l'icone dans la div puis ajout de la class appropriée
            let nbr = document.createElement('a');
            nbr.setAttribute('class', 'fas');
            //Ajout de l'icone à la card et ajout de la class _visuelle_
            nbr.innerHTML = item.nbr;

            this.grid.appendChild(card);
            card.appendChild(front);
            card.appendChild(back);
            back.appendChild(nbr);
        });
    }

    match() {
        //récupération des class 'selected'
        this.selected = document.querySelectorAll('.selected');
        //Ajout de la class 'match' à chacunes d'elles
        this.selected.forEach(card => {
            card.classList.add('match');
        });
        //Gestion de la victoire
        if(this.matchedCards.length == 10) {
            this.congratulation();
        }
    }

    unmatch() {
        //récupération des class 'selected'
        this.selected = document.querySelectorAll('.selected');
        //Ajout de la class 'unmatch' à chacunes d'elles
        this.selected.forEach(card => {
            card.classList.add('unmatch');
        });
    }

    resetChoise() {
        //reset des variables
        this.count = 0;
        this.firstChoise = '';
        this.secondChoise = '';
        //récupération des class 'selected'
        this.selected = document.querySelectorAll('.selected');
        //Suppression de la class 'unmatch' à chacunes d'elles
        this.selected.forEach(card => {
            card.classList.remove('selected', 'unmatch');
        });
    }

    //Méthode affichant la victoire
    congratulation(){
        //récupération du temps total
        let finalTime = this.timer.innerHTML;

        alert(this.moves+'coups en'+finalTime);
    }

    //Méthode pour rejouer
    playAgain() {
        window.location.reload();
    }

    //Méthode du compteur de coups
    startCounter() {
        this.moves++;
        this.counter.innerHTML = this.moves;

        if(this.moves == 1){
            this.startTimer();
        }
    }

    //Méthode du timer
    startTimer() {
        //interval toutes les secondes
        this.interval = setInterval(() =>{
            //Affichage
            this.timer.innerHTML= this.minute+":"+this.second;
            this.second++;
            //Incrémente minute toutes les 60s
            if(this.second == 60){
                this.minute++;
                this.second = 0;
            }
            //Incrémente hour toutes les 60m
            if(this.minute == 60){
                this.hour++;
                this.minute = 0;
            }

        }, 1000);
    }

    start() {
        this.grid.addEventListener('click', event =>  {

            let clicked = event.target;

            if(
                clicked.nodeName === "SECTION" || 
                clicked === this.previousTarget ||
                clicked.parentNode.classList.contains('selected')
            ){
                return;
            }

            if (this.count < 2) {
                this.count++;

                //Premier essai
                if(this.count === 1) {
                    //récupération du data-name
                    this.firstChoice = clicked.parentNode.dataset.type; 
                    //ajout de la class 'selected'
                    clicked.parentNode.classList.add('selected');
                    //start le compteur
                    this.startCounter();

                } else  {
                    //deuxième essai
                    //récupération du data-name
                    this.secondChoice = clicked.parentNode.dataset.type;
                    //ajout de la class 'selected'
                    clicked.parentNode.classList.add('selected');
                }

                //Si nos choix sont pas vides
                if(this.firstChoice !== '' && this.secondChoice !== '') {

                    if (this.firstChoice === this.secondChoice) {
                        this.match();
                        this.resetChoise();

                    } else {
                        this.unmatch();
                        setTimeout(this.resetChoise.bind(this), 1000);
                        //this.resetChoise();
                    }
                }   
                this.previousTarget = clicked;     
            }
        });
    }
}

let letsPlay = new game;
letsPlay.start();